<?php
/*
 * index.php is Yii application entry point of site script
 *
 * PHP version 5
*/ 
require_once dirname(__FILE__) . '/protected/environment/env.php';

Yii::createWebApplication($config)->run();
 
