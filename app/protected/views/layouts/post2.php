<?php $this->beginContent('//layouts/yiistrap'); ?>

        <div class="row-fluid">

            <div class="span9">
                <div class="well"><!---->
                    <?php echo $content; ?>
                </div><!---->
            </div>

            <?php 
                    if (!Yii::app()->user->isGuest)
                            $this->renderPartial('//layouts/partial/_sidebar');
            ?>
        </div>

<?php $this->endContent(); ?>
