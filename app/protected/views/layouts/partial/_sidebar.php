<?php

        $items  = array(
                array('label' => BlogModule::t("Manage Posts")),
                array('label' => '-----------------------------------'),
                array(
                        'label'     => BlogModule::t("Create Post"),
                        'url'       => array('create', 'parent_id'=>0),
                        'visible'   => !Yii::app()->user->isGuest
                ),
        	array(
                        'label'     => BlogModule::t("Manage Posts"),
                        'url'       => array('admin'),
                        'visible'   => Yii::app()->user->name == 'admin',
                )
        );

?>

<div class="span3">
    <div class="well">
        <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'             => $items,
                    'htmlOptions'       => array('class' => 'nav nav-list'),
                    'firstItemCssClass' => 'nav-header',
                    'activeCssClass'    => 'active',
                ));
        ?>
    </div>
</div>


