<?php

$items = array(   
        array('label'       => BlogModule::t('Rights'),
                'url'       => Yii::app()->urlManager->createUrl('rights/'),
                'visible'   => Yii::app()->user->IsSuperuser()),
    
        array('label'       => Yii::app()->getModule('user')->t("Yuor Profile"),
                'url'       => Yii::app()->getModule('user')->profileUrl,
                'visible'   => !Yii::app()->user->isGuest),
    
        array('label'       => Yii::app()->getModule('user')->t("Register"),
                'url'       => Yii::app()->getModule('user')->registrationUrl,
                'visible'   => Yii::app()->user->isGuest), 
    
        array('label'       => UserModule::t('Login'),
                'url'       => Yii::app()->urlManager->createUrl('user/login'),
                'visible'   => Yii::app()->user->isGuest),
    
        array('label'       => UserModule::t('Logout') . ' (' . Yii::app()->user->name . ')',
                'url'       => Yii::app()->urlManager->createUrl('user/logout'),
                'visible'   => !Yii::app()->user->isGuest)
);
?>

<div class="navbar-inner">
    <div class="container">
        <span class="brand"><a href="blog"><?php echo CHtml::encode(Yii::app()->name); ?></a></span>
        <?php
        
            $this->widget('zii.widgets.CMenu', array(
                
                    'items'         => $items,
                    'activeCssClass'=> 'active',
                    'htmlOptions'   => array(
                                'class' => 'nav',
                    ),
            ));
        ?>
    </div>
</div>


