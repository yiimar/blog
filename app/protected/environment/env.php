<?php
/**
 * Environment script
 * 
 * Shlang, 2013
 */
$dir = dirname(__FILE__);
        require_once $dir . DIRECTORY_SEPARATOR . 'definitions.php';
        require_once $dir . DS .'envboot.php';

if (defined('ENVIRONMENT') || (ENVIRONMENT == ENV_LOCAL)) {
        defined('YII_DEBUG')       || define('YII_DEBUG', true);
        defined('YII_TRACE_LEVEL') || define('YII_TRACE_LEVEL', 3);
};

$config = CONFIG_PATH . DS . 'main.php';

require_once YII_FRAMEWORK_PATH . DS . 'yii.php';        

if (!ini_get('date.timezone'))
        date_default_timezone_set('UTC');

date_default_timezone_set('Europe/Kiev');
