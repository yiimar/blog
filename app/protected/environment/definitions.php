<?php

/**
 * Parametres (const) environment definitions
 * 
 * Shlang, 2013
 */
define('DS', DIRECTORY_SEPARATOR);

define('BASE_PATH', dirname(__FILE__) . DS . '..');
        define('CONFIG_PATH',  BASE_PATH . DS . 'config');
        define('RUNTIME_PATH', BASE_PATH . DS . 'runtime');
        define('MODULES_PATH', BASE_PATH . DS . 'modules');        
                define('SYS_PATH', MODULES_PATH . DS . 'sys');
                        define('VENDORS_PATH',    SYS_PATH . DS . 'vendors');
                                define ('YII_FRAMEWORK_PATH',   VENDORS_PATH . DS . 'framework');
                                define ('PACKAGES_PATH',        VENDORS_PATH . DS . 'packages');
        
        define('APP_PATH',     BASE_PATH . DS . '..');        
                define('ASSETS_PATH', APP_PATH . DS . 'assets');                
                define('EXTERNAL_ASSETS', true);
                        
define('ENV_LOCAL',         'local');
define('ENV_PRODUCTION',    'production');
define('ENV_PREPRODUCTION', 'preproduction');