<?php
$title = BlogModule::t('About');

$this->pageTitle=Yii::app()->name . ' - ' . $title;
$this->breadcrumbs=array(
	$title,
);
?>
<div class="span11">
    <div class ="well">
        <h1><?= $title ?></h1>

        <p>This is the "about" page for my blog site.</p>
    </div>
</div>