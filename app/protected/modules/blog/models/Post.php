<?php

/**
 * This is the model class for table "{{post}}".
 *
 * The followings are the available columns in table '{{post}}':
 * @property integer $id
 * @property string $body
 * @property integer $marked
 * @property string $create_time
 * @property integer $author_id
 *
 * Shlang, 2013
 */
class Post extends CActiveRecord
{
        const MARKED_SET    = 1;
        const MARKED_UN_SET = 0;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{post}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('body',       'required'),
                        array('body',       'length',       'max' => 500),
                        array('marked',     'boolean'),
                        array('marked',     'safe', 'on'=>'marked'),
			array('author_id',  'numerical',    'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, body, marked, create_time, author_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author'    => array(self::BELONGS_TO,  'User', 'author_id'),
                        'subNodes'  => array(self::HAS_MANY,    'Node', 'parent_id',
                                        'order' => 'subNodes.post.create_time ASC')
		);
	}
        
        /**
         * Using CTimestampBehavior for cteate post time setting
         * 
         * @return type array of behavior config
         */
        public function behaviors()
        {
                return array(
                        'CTimestampBehavior' => array(
                                'class'             => 'zii.behaviors.CTimestampBehavior',
                                'createAttribute'   => 'create_time',
                                'setUpdateOnCreate' => false,                            
                                'updateAttribute'   => null,    // Timestamp for update post not used
                        )
                );
        }
        
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'            => BlogModule::t("Id"),
			'body'          => BlogModule::t("Body"),
			'marked'        => BlogModule::t("Marked"),
			'create_time'   => BlogModule::t("Create Time"),
			'author_id'     => BlogModule::t("Author"),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',            $this->id);
		$criteria->compare('body',          $this->body,true);
		$criteria->compare('marked',        $this->marked);
		$criteria->compare('create_time',   $this->create_time,true);
		$criteria->compare('author_id',     $this->author_id);

		return new CActiveDataProvider($this, array(
			'criteria'  => $criteria,
                        'Pagination'=> array('PageSize' => 5),
		));
	}
        
        /**
         * Delete according post node
         */
        protected function afterDelete()
        {
                parent::afterDelete();
                
                Node::model()->deleteAll(array('condition'=>'post_id='.$this->id));
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
