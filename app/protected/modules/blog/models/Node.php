<?php

/**
 * This is the model class for table "{{node}}".
 *
 * The followings are the available columns in table '{{node}}':
 * @property integer $id
 * @property integer $post_id
 * @property integer $parent_id
 *
 * Shlang, 2013
 */
class Node extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{node}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, post_id, parent_id', 'required'),
			array('id, post_id, parent_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, post_id, parent_id', 'safe',      'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'post' => array(self::BELONGS_TO, 'Post', 'post_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'        => BlogModule::t("Node Id"),
			'post_id'   => BlogModule::t("Id"),
			'parent_id' => BlogModule::t("Parent"),
		);
	}
        
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('post_id',$this->post_id);
		$criteria->compare('parent_id',$this->parent_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}        
        
        public function getBranch($id)
        {
                $this->getDbCriteria()->mergeWith(array(
                        'select'    => array('id, post_id'),
                        'condition' => 'parent_id=:parent_id',
                        'params'    => array(':parent_id'=>$id),
                ));
                return $this;
        }        

        /**
         * Удаление ветки (поддерева), начинающегося от узла id
         * 
         * @param type $id - номер корневого узла удаляемой ветви
         */
        public function deleteBranchByNodeId($id)
        {                
                $levelNodes = $this->getBranch($id)->findAll();
                
                if (count($levelNodes)!==0) {
                        foreach ($levelNodes as $level) {
                                $this->deleteBranchByNodeId($level->id);
                                Node::model()->deleteByPk($level->id);
                                Post::model()->deleteByPk($level->post_id);                                
                        }
                }
        }
        
        public static function saveDataAsHtml($data)
        {
                $html='';
                if(is_array($data))
                {
                        foreach($data as $node)
                        {
                                $html .= CHtml::tag('li',false);
                                if(!empty($node['children']))
                                {
                                        $html .= "\n<ul>\n";
                                        $html .= self::saveDataAsHtml($node['children']);
                                        $html .= "</ul>\n";
                                }
                                $html .= $node->post->body . "\n";
                                $html .= CHtml::closeTag('li') . "\n";
                        }
                }
                return $html;
        }       

        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Node the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
