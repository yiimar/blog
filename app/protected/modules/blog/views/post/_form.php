<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'post-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?= BlogModule::t('Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,   BlogModule::t('body')); ?>
		<?php echo $form->textArea($model,  'body',   array('rows'=>6, 'cols'=>80)); ?>
		<?php echo $form->error($model,     'body'); ?>
	</div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord 
                                        ? BlogModule::t('Create') 
                                        : BlogModule::t('Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->