<?php
/* @var $this PostController */
/* @var $model Post */
$title = BlogModule::t("Create Post");
$this->breadcrumbs=array(
	BlogModule::t("Manage Posts") => array('admin'),
	$title,
);

$this->menu=array(
	array('label' => BlogModule::t("Manage Posts"), 'url'=>array('admin')),
);
?>

<h1><?php echo $title ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>
