<?php
/* 
 * @var $this PostController 
 */

$title = BlogModule::t("Posts");
$this->breadcrumbs=array(
	$title,
);
echo CHtml::openTag('h1'), CHtml::encode($title), CHtml::closeTag('h1');

$this->widget('PostTreeView');
