<!--
/* @var $this PostController */
/* @var $data Post */
-->

<div class="view">
    <div class="post-content">
        <?php echo CHtml::encode($data->content); ?>
    </div>
    <div class="under-block">
        <div class="post-author">
            <?php echo CHtml::encode($data->author->username); ?>
        </div>
        
        <?php if (Yii::app()->user->username=='moderator') : ?>
                <div class="right-control-block">
                    	<div class="row buttons">
                            <?php $this->renderPartial('marked',array('data'=>$data),false,true); ?>
                        </div>
                </div>
        <?php endif ?>
        
        <div class="right-control-block">
               	<div class="row buttons">
                    <?php echo CHtml::submitButton(BlogModule::t("Add Post"),
                               array('submit' => array('createNode', ))); ?>
                </div>
        </div>
    </div>
</div>