<?php
/* @var $this PostController */
/* @var $dataProvider CActiveDataProvider */
$title = BlogModule::t("Posts");

$this->breadcrumbs=array(
	$title,
);

$this->menu=array(
        array('label' => BlogModule::t("Manage Posts")),
        array('label' => '-----------------------------------'),
	array('label' => BlogModule::t("Create Post"),  'url'=>array('create')),
	array('label' => BlogModule::t("Manage Posts"), 'url'=>array('admin')),
);
print_r($posts);
echo CHtml::openTag('h1'), CHtml::encode($title), CHtml::closeTag('h1');

$this->widget('PostTreeView'/*, array('posts' => $posts)*/);
