<?php
/**
 * @var $this PostController
 * 
 * @var $model Post
 * 
 * 
 */
$title = BlogModule::t("Manage Posts");
$this->breadcrumbs=array($title,);

$this->menu=array(
        array('label'=> BlogModule::t("Manage Posts")),
        array('label'=> '-----------------------------------'),
	array('label'=> BlogModule::t("List Post"),   'url' => array('index')),
	array('label'=> BlogModule::t("Create Post"), 'url' => array('create')),
);

?>

<h1><?= $title ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'post-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
                    'name'        => 'id',
                    'value'       => '$data->id',
                    'htmlOptions' => array(
                            'style' => 'text-align:center',
                            'width' => '30px',
                    ),                  
                ),
		array(
                    'name'        => 'content',
                    'header'      => BlogModule::t("Content"),               
                    'filter'      => false,
                    'value'       => '$data->content',
                    'htmlOptions' => array(
                            'style' => 'text-align:justify',                      
                    ),                  
                ),
                array(
                    'name'        => 'marked',
                    'filter'      => Lookup::items('PostMarked'),
                    'value'       => 'Lookup::item("PostMarked",$data->marked)', 
                    'htmlOptions' => array(
                            'style' => 'text-align:center',                      
                    ),                  
                ),
		array(
                    'name'        => 'create_time',
                    'filter'      => false,  
                    'value'       => '$data->create_time',
                    'htmlOptions' => array(
                            'style' => 'text-align:center',                      
                    ),                  
                ),
		array(
                    'name'        => 'author_id',
                    'header'      => BlogModule::t("Author"),               
                    'filter'      => CHtml::listData(User::model()->findAll(), 'id', 'username'),  
                    'value'       => '$data->author->username',
                    'htmlOptions' => array(
                            'style' => 'text-align:center',                      
                    ),                  
                ),
		array(
                    'name'        => 'parent_id',
                    'value'       => '$data->parent_id',
                    'htmlOptions' => array(
                            'style' => 'text-align:center',                      
                    ),                  
                ),
		array(
                    'class'       => 'CButtonColumn',
		),
	),
)); ?>
