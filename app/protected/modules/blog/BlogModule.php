<?php

class BlogModule extends CWebModule
{
      	private $_assetsUrl;
	public $cssFile;
        
      	public $tablePost   = '{{post}}';
	public $tableNode   = '{{node}}';
	public $tableLookUp = '{{lookup}}';
    
	public function init()
	{
                $this->defaultController = 'post';
                
                Yii::app()->clientScript->registerCssFile(
                        Yii::app()->assetManager->publish(
                                Yii::getPathOfAlias('blog.assets')
                        ).'/default.css',
                        CClientScript::POS_END
                );
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
        
        /**
	 * @param $str
	 * @param $params
	 * @param $dic
	 * @return string
	 */
	public static function t($str='',$params=array(),$dic='post') 
        {
		if (Yii::t("BlogModule", $str)==$str)   return Yii::t("BlogModule.".$dic, $str, $params);
                else                                    return Yii::t("BlogModule", $str, $params);
	}

}
