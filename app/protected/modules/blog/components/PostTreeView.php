<?php

/**
 * Description of PostTree
 *
 * @author shlang
 */
class PostTreeView extends CWidget
{
 	public function init()
	{
//                Yii::app()->clientScript->registerCssFile(
//                        Yii::app()->assetManager->publish(
//                                Yii::getPathOfAlias('blog.assets')
//                        ).'/default.css',
//                        CClientScript::POS_END
//                );

                $this->renderContent(0);
	}

	protected function renderContent($id)
	{
                $nodes = Node::model()->getBranch($id)->findAll();
                
                if (!empty($nodes)) {
                        echo CHtml::openTag('ul');
                                    foreach ($nodes as $node) {
                                            $post = Post::model()->findByPk($node->post_id);
                                            
                                            echo CHtml::openTag('li');
                                            $this->render('postTreeView', array('post' => $post));                               
                                            echo CHtml::closeTag('li');
                                            
                                            $this->renderContent($node->id);                                            
                                    }
                        echo CHtml::closeTag('ul');
                }
	}
}
