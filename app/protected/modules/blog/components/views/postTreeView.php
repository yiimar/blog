<div class="well well-large <?php echo $post->marked ? 'text-error' : '' ?>">
    <?php echo TbHtml::well($post->body, array('size' => TbHtml::WELL_SIZE_LARGE,)); ?>

    <div class="btn-toolbar" style="margin: 0;">
        <div class="pull-left">

        </div>
        <span class="label label-info">
            <?php
            echo CHtml::encode($post->author->username) . "<br><br>"
            . CHtml::encode($post->create_time);
            ?>
        </span> 
        <div class="pull-right">
            <div class="btn-group">
                <?php if (Yii::app()->user->checkAccess('Moderator')) : ?>
                    <div class="btn-group">
                        <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            <?= BlogModule::t('Marked') ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li>        
                                <?php
                                // Макрируем пост
                                echo CHtml::link(BlogModule::t("SetMarked Post"), array(
                                    'mark',
                                    'id' => $post->id,
                                    'marked' => Post::MARKED_SET
                                ));
                                ?>
                            </li>
                            <li>        
                                <?php
                                // снимаем маркировку
                                echo CHtml::link(BlogModule::t("UnMarked Post"), array(
                                    'mark',
                                    'id' => $post->id,
                                    'marked' => Post::MARKED_UN_SET
                                ));
                                ?>
                            </li>
                        </ul>
                    </div> 

                <?php
                endif;

                if (!Yii::app()->user->isGuest) ://
                    echo TbHtml::button(BlogModule::t("Create1 Post"), array(
                        "submit" => array("create", "parent_id" => $post->id),
                        'class' => 'btn-success',
                    ));
                endif;
                ?>

            </div>    
        </div>
    </div>
</div>
