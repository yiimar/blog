-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 22 2013 г., 16:30
-- Версия сервера: 5.5.32
-- Версия PHP: 5.3.10-1ubuntu3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `AuthAssignment`
--

CREATE TABLE IF NOT EXISTS `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AuthAssignment`
--

INSERT INTO `AuthAssignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('Admin', '1', NULL, 'N;'),
('Authenticated', '2', NULL, 'N;'),
('Moderator', '3', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `AuthItem`
--

CREATE TABLE IF NOT EXISTS `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AuthItem`
--

INSERT INTO `AuthItem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('Admin', 2, 'Admin', NULL, 'N;'),
('Authenticated', 2, 'Authenticated', NULL, 'N;'),
('Blog.Post.Admin', 0, NULL, NULL, 'N;'),
('Blog.Post.Create', 0, NULL, NULL, 'N;'),
('Blog.Post.Delete', 0, NULL, NULL, 'N;'),
('Blog.Post.Index', 0, NULL, NULL, 'N;'),
('Blog.Post.Mark', 0, NULL, NULL, 'N;'),
('Blog.Post.Update', 0, NULL, NULL, 'N;'),
('Blog.Post.View', 0, NULL, NULL, 'N;'),
('Guest', 2, 'Guest', NULL, 'N;'),
('Moderator', 2, 'Moderator', NULL, 'N;'),
('updateOwnPost', 1, 'Update only own Post', 'return Yii::app()->user->id==Post::model()->findByPk($_GET[''id''])->author_id;', 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `AuthItemChild`
--

CREATE TABLE IF NOT EXISTS `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AuthItemChild`
--

INSERT INTO `AuthItemChild` (`parent`, `child`) VALUES
('Authenticated', 'Blog.Post.Create'),
('Guest', 'Blog.Post.Index'),
('Moderator', 'Blog.Post.Mark'),
('updateOwnPost', 'Blog.Post.Update'),
('Guest', 'Blog.Post.View'),
('Authenticated', 'Guest'),
('Moderator', 'Guest'),
('Authenticated', 'updateOwnPost');

-- --------------------------------------------------------

--
-- Структура таблицы `blog_lookup`
--

CREATE TABLE IF NOT EXISTS `blog_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `code` int(11) NOT NULL,
  `type` varchar(128) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `blog_lookup`
--

INSERT INTO `blog_lookup` (`id`, `name`, `code`, `type`, `position`) VALUES
(1, 'Маркировано', 1, 'PostMarked', 1),
(2, 'Не маркировано', 0, 'PostMarked', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_node`
--

CREATE TABLE IF NOT EXISTS `blog_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `blog_node`
--

INSERT INTO `blog_node` (`id`, `post_id`, `parent_id`) VALUES
(1, 1, 0),
(2, 2, 0),
(3, 3, 1),
(4, 4, 1),
(5, 5, 2),
(6, 6, 3),
(7, 7, 6),
(8, 8, 2),
(9, 9, 0),
(10, 10, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_post`
--

CREATE TABLE IF NOT EXISTS `blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` text NOT NULL,
  `marked` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_post_author` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `blog_post`
--

INSERT INTO `blog_post` (`id`, `body`, `marked`, `create_time`, `author_id`) VALUES
(1, 'You see the two rules at the end, one changes the modified field when the record''s being updated, and the other changes both fields when the record''s being created. You''ll also see the "new CDbExpression(''NOW()'')" statement. This passes "NOW()" to the MySQL server and it will not be escaped. MySQL will interpret it as a statement and not as a string. This means that the field types could have been any other date/time type (timestamp, etc.) and it would still work.\r\n1111111111', 0, '2013-11-20 21:19:07', 1),
(2, 'Note that in the above code, when creating a new record only the ''created'' field will be assigned/updated, while the ''modified'' record will be assigned/updated only when updating an existing record.\r\n\r\nIf you want to assign/update the ''modified'' field even when creating a new record... use this code:\r\n2222222222222222', 0, '2013-11-20 21:20:15', 1),
(3, 'These are simple and elegant solutions to this issue.\r\n\r\nThe third possibility is to use CTimestampBehavior in your models. You can read about this in the API documentation: http://www.yiiframework.com/doc/api/1.1/CTimestampBehavior\r\n3333333333333333', 0, '2013-11-20 21:21:31', 1),
(4, 'Разумеется, этот код сопровождается валидацией и обработкой ошибок, а так же может быть заключен в трансакцию. Чего бы мне хотелось — так это сделать универсальный код для сохранения по разному связанных между собой моделей. \r\n\r\n Например, необходимо сохранить такие данные с формы: нового клиента, у которого есть 1 адрес, и много контактных лиц; в то же время, клиент связан с создаваемым заказом и счетом (invoice); а заказ в свою очередь связан со счетом. В итоге, эти все модели можно сохранить так:\r\n4444444444444444444444444', 0, '2013-11-20 21:23:30', 1),
(5, 'Я не так давно написал компонент, в котором реализовал сохранение связанных записей (CActiveRecord) и хотел бы поделиться этим кодом. \r\n\r\n Я заметил, что часто пишется повторяющийся код, когда, например, нужно сохранить даные о клиенте со всеми его контактами, то пишется что-то типа такого (по крайней мере, я так писал):\r\n5555555555555555', 0, '2013-11-20 21:24:05', 1),
(6, 'I''m trying to have 50 or more items listed in CGridView-Widget with following code, but it doesn''t effect.\r\n Can someone tell me why it doesn''t effect and how to fix it?\r\n Many thanks in advance.\r\n66666666666666', 1, '2013-11-20 21:29:17', 1),
(7, 'И мы можем писать именно так, потому что $this в данном случае — это экземпляр класса Controller от которого наследуются все наши контроллеры и который передается во вьюшки и лейауты.\r\n\r\n С виджетами дело обстоит немного по-другому, нужен свой подход, поскольку вьюшки виджета выполняются не в контексте контроллера, а в контексте виждета, поэтому для подключения Javascript файла во вьюшке виджета будем писать:', 0, '2013-11-20 23:56:53', 1),
(8, 'втогенерируемые имена подпапок в папочке /assets в корне веб-приложения основываются на хеше имени папки /protected/assets и не меняются от раза к разу, даже если вы чистите папочку /assets в корне веб-приложения. Они восстанавливаются в прежнем виде. А значит браузер пользователя не «просекает», что что-то поменялось до тех пор, пока не истечет время кеширования.', 0, '2013-11-20 23:57:47', 1),
(9, 'В случае, если вы используете фреймворк Yii, вы также можете указывать версии или временные метки у файлов скриптов и стилей при подключении, однако за этим постоянно нужно следить, а в случае Yii еще и следить за отсутствием конфликтов (когда, допустим, виджет и вьюшка используют один и тот же скрипт, но с разными временными метками).', 0, '2013-11-21 10:55:15', 1),
(10, 'Я вас молю – утешьтесь, государь!\r\n Спасенью радоваться надо больше,\r\n Чем горевать о тягостных утратах. \r\nШекспир Уильям - Буря', 0, '2013-11-21 11:26:41', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_post1`
--

CREATE TABLE IF NOT EXISTS `blog_post1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `marked` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author_id` int(11) NOT NULL,
  `root` int(10) NOT NULL,
  `lft` int(10) NOT NULL,
  `rgt` int(10) NOT NULL,
  `level` smallint(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_post_author` (`author_id`),
  KEY `root` (`root`),
  KEY `lft` (`lft`),
  KEY `rgt` (`rgt`),
  KEY `level` (`level`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `blog_post1`
--

INSERT INTO `blog_post1` (`id`, `content`, `marked`, `create_time`, `author_id`, `root`, `lft`, `rgt`, `level`) VALUES
(1, 'Чтобы расширение "понимало" с одним или многими деревьями мы собираемся работать следует использовать параметр конфигурации hasManyRoots, который по умолчанию = false\r\nОсновные методы и функции перечислены здесь\r\nKey Words for FKN + antitotal forum (CS VSU):', 0, '0000-00-00 00:00:00', 1, 1, 1, 2, 1),
(2, 'Для начала качаем само расширение - как и написано на странице (http://www.yiiframework.com/extension/ne...) самый свежачок располагается на гитхаб-хостинге расширения(https://github.com/yiiext/nested-set-beh...) качаем оттуда NestedSetBehavior.php (ну или копипастим исходный код)\r\n кидаем это файл в папку protected/components/ при этом должна быть настроена  автоматическая подгрузка компонентов из данной папки.', 0, '0000-00-00 00:00:00', 1, 2, 1, 2, 1),
(3, 'где\r\n?1\r\n2\r\n3	''leftAttribute''=>''lft'',\r\n          ''rightAttribute''=>''rgt'',\r\n          ''levelAttribute''=>''level'',\r\n\r\n\r\nуказывают на имена столбцом для соответствующих индексов (левого, правого ключа и номера "глубины" - уровня иерархии) - поля с данными именами должны присутствовать в таблице модели.\r\n\r\nВ частности - по умолчанию предлагается использовать такие схемы (пример для некоторой сущности "категория") -\r\n для хранения многих деревьев в одной таблице:', 0, '0000-00-00 00:00:00', 1, 3, 1, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_profiles`
--

CREATE TABLE IF NOT EXISTS `blog_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `blog_profiles`
--

INSERT INTO `blog_profiles` (`user_id`, `lastname`, `firstname`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Demo', 'Demo'),
(3, 'Shlang84', 'Shlang');

-- --------------------------------------------------------

--
-- Структура таблицы `blog_profiles_fields`
--

CREATE TABLE IF NOT EXISTS `blog_profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `blog_profiles_fields`
--

INSERT INTO `blog_profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', '50', '3', 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_users`
--

CREATE TABLE IF NOT EXISTS `blog_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `blog_users`
--

INSERT INTO `blog_users` (`id`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit_at`, `superuser`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', '9a24eff8c15a6a141ece27eb6947da0f', '2013-11-11 20:49:10', '2013-11-22 14:25:41', 1, 1),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', '2013-11-11 20:49:10', '2013-11-22 14:24:57', 0, 1),
(3, 'moder', '81dc9bdb52d04dc20036dbd8313ed055', 'mfc.consult@mail.ru', 'ca0e84d8b0f870803c849d32cd0b3af0', '2013-11-22 09:46:40', '2013-11-22 14:21:17', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `Rights`
--

CREATE TABLE IF NOT EXISTS `Rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `AuthAssignment`
--
ALTER TABLE `AuthAssignment`
  ADD CONSTRAINT `AuthAssignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `AuthItemChild`
--
ALTER TABLE `AuthItemChild`
  ADD CONSTRAINT `AuthItemChild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AuthItemChild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `blog_post1`
--
ALTER TABLE `blog_post1`
  ADD CONSTRAINT `FK_post_author` FOREIGN KEY (`author_id`) REFERENCES `blog_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `blog_profiles`
--
ALTER TABLE `blog_profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `blog_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `Rights`
--
ALTER TABLE `Rights`
  ADD CONSTRAINT `Rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
