<?php

class m131122_151158_create_node_table extends CDbMigration
{
	public function safeUp()
	{
                $this->createTable(Yii::app()->getModule('blog')->tableNode, array(
                        'id'        => 'pk',
                        'post_id'   => 'int(11) NOT NULL',
                        'parent_id' => 'int(11) NOT NULL',
                ));
                $this->addPrimaryKey('id', Yii::app()->getModule('blog')->tableNode, 'id');

	}

	public function safeDown()
	{
		$this->dropTable(Yii::app()->getModule('blog')->tableNode);
	}
}