<?php

class m131122_151636_create_lookup_table extends CDbMigration
{
	public function safeUp()
	{
                $this->createTable(Yii::app()->getModule('blog')->tableLookUp, array(
                        'id'        => 'pk',
                        'name'      => 'varchar(11) NOT NULL',
                        'code'      => 'int(11) NOT NULL',
                        'type'      => 'varchar(11) NOT NULL',
                        'position'  => 'int(11) NOT NULL',
                ));
                
                $this->addPrimaryKey('id', Yii::app()->getModule('blog')->tableLookUp, 'id');
                
                $this->insert(Yii::app()->getModule('blog')->tableLookUp, array(
                        'id'        => 1,
                        'name'      => 'Маркировано',
                        'code'      => 1,
                        'type'      => 'PostMarked',
                        'position'  => '1',                        
                ));
                $this->insert(Yii::app()->getModule('blog')->tableLookUp, array(
                        'id'        => 2,
                        'name'      => 'Не маркировано',
                        'code'      => 0,
                        'type'      => 'PostMarked',
                        'position'  => '1',                        
                ));                
	}

	public function safeDown()
	{
		$this->dropTable(Yii::app()->getModule('blog')->tableLookUp);
	}
}