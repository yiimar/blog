<?php

class m131122_151119_create_post_table extends CDbMigration
{
	public function safeUp()
	{
                $this->createTable('blog_post', array(
                        'id'            => 'pk',
                        'body'          => 'text NOT NULL',
                        `marked`        => 'tinyint(1) NOT NULL DEFAULT 0',
                        `create_time`   => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
                        `author_id`     => 'int(11) NOT NULL',
                ));
                
                $this->insert('blog_post', array(
                        'id'            => '0',
                        'body'          => '',
                        `marked`        => '0',
                        `author_id`     => '1',
                ));
                $this->addPrimaryKey('id', Yii::app()->getModule('blog')->tablePost, 'id');
	}        

	public function safeDown()
	{
		$this->dropTable(Yii::app()->getModule('blog')->tablePost);
	}
}
