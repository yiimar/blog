<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'blog_',
  'modelPath' => 'application.modules.blog.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '1',
);
