<?php

/**
 * Application-level aliases
 *
 * PHP version 5
 *
 * Shlang 2013
 */
return array(
        /**
         * 'bootstrap' - Yiistrap 1.2.0
         */
        'bootstrap'         => realpath(VENDORS_PATH . DS . 
//                                                            'booster'
                                                            'yiistrap'
                               ),
        /**
         * Modules aliases
         */
        'blog'              => realpath(MODULES_PATH . DS . 'blog'),
        'user'              => realpath(MODULES_PATH . DS . 'user'),
        'rights'            => realpath(MODULES_PATH . DS . 'rights'),
        'sys'               => realpath(MODULES_PATH . DS . 'sys'),
    
        /**
         * yiiext-nested-set-behavior-3d52b71
         */
        'nestedSetBehavior' => realpath(VENDORS_PATH . DS . 'nested-set-behavior'),
        /**
         * 'yii-debug-toolbar'
         */
         'yii-debug-toolbar'=> realpath(VENDORS_PATH . DS . 'yii-debug-toolbar'),
         
);
