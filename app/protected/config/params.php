<?php

/**
 * Application-level parameters that can be accessed configuration
 *          using Yii::app()->params['paramName']
 *
 * PHP version 5
 *
 * Shlang 2013
 */
return array(
        // this is used in contact page
        'adminEmail' => 'info@my.ru',
);
