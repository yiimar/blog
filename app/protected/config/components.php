<?php

/**
 * Components configuration
 *
 * PHP version 5
 *
 * Shlang   2013
 */
return array(
        'widgetFactory' => array(
                'enableSkin'    => true,
        ),
        'user'          => array(
                // enable cookie-based authentication
                'class'         => 'RWebUser',
                'allowAutoLogin'=> true,
                'loginUrl'      => array('/user/login'),
        ),
        'bootstrap' => array(
//                'class'          => 'bootstrap.components.Bootstrap',
                'class'          => 'bootstrap.components.TbApi',
//                'coreCss'        => false,
//                'responsiveCss'  => false,
//                'yiiCss'         => false,
//                'jqueryCss'      => false,
//                'enableJS'       => false,
//                'fontAwesomeCss' => false,
        ),
    
//      'rights',
        'authManager'   => array(
                 'class'        => 'RDbAuthManager',
                 'connectionID' => 'db',
                 'defaultRoles' => array('Authenticated', 'Guest'),
        ),
        'authorizer'    => array(
		'class'         => 'RAuthorizer',
		'superuserName' => 'admin',
	),
	'generator'     => array(
		'class'         => 'RGenerator',
	),
    
        'cache'         => array(
                'class'         => 'system.caching.CFileCache',
        ),
    
        'urlManager'    => array(
                'urlFormat'      => 'path',
                'showScriptName' => false,
                'rules'          => include_once CONFIG_PATH . '/rules.php',
        ),    
				
        'errorHandler'  => array(
                // use 'site/error' action to display errors
                'errorAction'=>'sys/site/error',
        ),
        'session'       => array(
               'sessionName' =>'MYSESSIONID'  
        ),  
    
//    'request' => array(
        //        'enableCookieValidation' => true,
//    ),
    
);
