<?php

/**
 * Yii Application import configuration
 *
 * PHP version 5
 * 
 * Shlang   2013
 */
return array(
        'application.models.*',
        'application.components.*',
        'application.controllers.*',
    
        'application.modules.sys.*',
        'application.modules.sys.models.*',
        'application.modules.sys.components.*',
        'application.modules.sys.controllers.*',
    
        'application.modules.user.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.user.controllers.*',
    
        'application.modules.rights.*',
        'application.modules.rights.models.*',
        'application.modules.rights.components.*',
        'application.modules.rights.components.behaviors*',
        'application.modules.rights.components.dataproviders.*',    
        'application.modules.rights.controllers.*',
    
        'application.modules.blog.*',
        'application.modules.blog.models.*',
        'application.modules.blog.components.*',
        'application.modules.blog.controllers.*',
    
        'bootstrap.helpers.TbHtml',
//        'bootstrap.components.*',

        'nestedSetBehavior.*'
);