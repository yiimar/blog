<?php
/**
 * Application modules configuration map
 *
 * PHP version 5
 *
 */
return array(    
        'sys',
        'user',
        'rights',
        'blog',
);