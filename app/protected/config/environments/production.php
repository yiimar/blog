<?php

/**
 * Components configuration for production
 *
 * PHP version 5
 *
 * Shlang, 2013
 */
return array(
        'components' => array(
                'db'        => array_merge(
                        require_once 'db_production.php',
                        array('schemaCachingDuration' => 108000)
                ),

                'log'       => array(
                        'class'     => 'CLogRouter',
                        'routes'    => array(
                                array(
                                        'class'  => 'CFileLogRoute',
                                        'levels' => 'error, warning, info, trace', // на продакшн лучше оставить error, warning
                                ),
                        ),
                ),
                'cache'     => array(
                        'class' => 'CApcCache',
                ),
                'session'   => array(
                        'class'     => 'CCacheHttpSession',
                        'timeout'   => 28800,
                ),
        ),
);
