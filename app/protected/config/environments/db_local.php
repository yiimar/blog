<?php

/**
 * DB Connection configuration local
 *
 * PHP version 5
 *
 * Shlang   2013
 **/
return array(
        'class'            => 'CDbConnection',
        'connectionString' => 'mysql:host=localhost;dbname=blog',
        'emulatePrepare'   => true,
    
        'username' => 'root',
        'password' => 'mysql',
    
        'charset' => 'utf8',
    
        'schemaCachingDuration' => 108000,
    
        'tablePrefix' => 'blog_',
    
        // включаем профайлер
        'enableProfiling'    => true,
        // показываем значения параметров
        'enableParamLogging' => true,
    
);
?>
