<?php

/**
 * Components configuration for local
 *
 * PHP version 5
 *
 * Shlang, 2013
 */
return array(
            'components' => array(
                    'db'    => include_once 'db_local.php',

                    'log'   => array(
                            'class'  => 'CLogRouter',
                            'routes' => array(
                                    array(
                                            'class' => 'yii-debug-toolbar.YiiDebugToolbarRoute',
                                    ),
                            ),
                    ),
            ),
    
            'modules'    => array(
                    'gii'   => array(
//                            'generatorPaths'=> array('bootstrap.gii'),
                            'class'         => 'system.gii.GiiModule',
                            'password'      => '111',
                    ),
            ),
);
