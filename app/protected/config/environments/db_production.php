<?php

/**
 * DB Connection configuration production
 *
 * PHP version 5
 *
 * @category domen
 * @package  domen.config
 **/
return array(
        'class'            => 'CDbConnection',
        'connectionString' =>'mysql:host=localhost;dbname=blog',
        'emulatePrepare'   => true,
    
        'username' => 'root',
        'password' => 'mysql',
    
        'charset' => 'utf8',
    
        'enableParamLogging' => 0,
        'enableProfiling'    => 0,
    
        'schemaCachingDuration' => 108000,
    
        'tablePrefix' => 'blog_',//
);
?>
