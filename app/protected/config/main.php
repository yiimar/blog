<?php

/**
 * This is the main Web application configuration. Any writable
 * CWebApplication properties can be configured here.
 * 
 * Shlang   2013
 */
ini_set('mbstring.internal_encoding', 'UTF8');

$env_file = CONFIG_PATH . DS . 'environments' . DS . ENVIRONMENT . '.php';
        $environment = is_file($env_file) ? include $env_file : array();

$conf = array(
    
        'name'              => 'Мини-БЛОГ',    
	'basePath'          => BASE_PATH,
    
        'language'          => 'ru',
        'sourceLanguage'    => 'en',
    
        'preload'           => array('log'),
    
        'aliases'    => require_once CONFIG_PATH . DS . 'aliases.php',
        'import'     => require_once CONFIG_PATH . DS . 'import.php',
        'params'     => require_once CONFIG_PATH . DS . 'params.php',
        'modules'    => require_once CONFIG_PATH . DS . 'modules.php',
        'components' => require_once CONFIG_PATH . DS . 'components.php',
    
        'defaultController' => 'blog/post',
        
);

return CMap::mergeArray($conf, $environment);